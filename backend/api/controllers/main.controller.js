var providers = require('../models/providers.models');
const Provider = require('../db/db');
const { ObjectId } = require('mongodb');


///UTil functions
//check if list is empty
function isEmptyList(obj) {
    return (!obj || obj.length == 0 || Object.keys(obj).length == 0);
}
//check for existing provider

// function existProvider(id){
//     return providers.find( provider => provider.id == id );
// }
//Genrate a unique ID
// function getUniqueId(providers){
//     let min= 100000;
//     let max= 999999;
//     do{
//     var id= Math.floor(Math.random() *(max-min) * min);
//     }while(existProvider(id));
//     return id;

// }
//CRUD Create (post), Read (Get). Update (put), Delete (delete)

// POST
//  uri /api/providers
// Handle errors
function handleErrors(res, error) {
    res.status(404);
    res.send("something wrong  \n " + error);
}

module.exports.create = function (req, res) {
    try {


        var provider = req.body;

        Provider.create(provider).then(result => {
            res.status(201);
            res.send(result);
        }).catch(error => handleErrors(res, error))

    } catch (error) { handleErrors(res, error) }

}

// GET
//  uri /api/providers



module.exports.readAll = function (req, res) {

    try {
        Provider.find({}).then(result => {

            if (isEmptyList(result)) {
                res.status(404);
                req.send("List is empty");
            }
            res.status(200);

            res.send(result);
        })
            .catch(error => handleErrors(res, error))
    } catch (error) {
        handleErrors(res, error);
    }


}

// GET ONE
//  uri /api/providers/123

module.exports.readOne = function (req, res) {
    try {
        let id = new ObjectId(req.params.id);
        console.log(id)
        Provider.find({ '_id': id })
            .then(result => {

                if (isEmptyList(result)) {
                    res.status(404);
                    res.send("List is empty");
                }

                //let provider = providers.find(provider => provider.id == id)
                res.status(200);
                res.send(result);

            }).catch(error => handleErrors(res, error))
    } catch (error) {
        handleErrors(res, error);
    }



}


// PUT
//  uri /api/providers/123

module.exports.update = function (req, res) {
    try {

        let id = new ObjectId(req.params.id);
        let provider = req.body;
        Provider.findByIdAndUpdate({ '_id': id }, provider, { new: true })
            .then(result => {
                if (isEmptyList(result)) {
                    res.status(404);
                    req.send("List is empty. Cannot update");
                }

                res.status(200);
                res.send(result);
            }).catch(error => handleErrors(res, error));


    } catch (error) {
        handleErrors(res, error);
    }


}

// DELETE
//  uri /api/providers/123

module.exports.deleteOne = function (req, res) {

    try {
        let id = new ObjectId(req.params.id);
        Provider.findByIdAndDelete({'_id':id}).then(result => {

            if (isEmptyList(result)) {
                res.status(404);
                req.send("List is empty. Cannot delete");
            }



            res.status(200); // res.status(404)
            res.send(result);

        }).catch(error => handleErrors(res, error))



    } catch (error) {
        handleErrors(res, error);
    }

}

// DELETE
//  uri /api/providers

module.exports.deleteAll = function (req, res) {
    try{
        Provider.deleteMany({}).then(result=>{
            if (result.deletedCount===0) {
                res.status(404);
                req.send("List is empty. Cannot delte");
            }
            providers = [];
            res.status(200); // res.status(404)
            res.send("All providers deleted /n");
        }).catch(error => handleErrors(res, error))
    
} catch (error) {
    handleErrors(res, error);
}
}
