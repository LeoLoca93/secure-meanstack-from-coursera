const mongoose = require('mongoose');

const {providerSchema} = require('../schemas/provider.schemas')

// Create providr model

const Provider = mongoose.model('Providers', providerSchema);

module.exports = {Provider}