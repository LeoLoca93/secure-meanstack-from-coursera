var express = require('express')
var router = express.Router();
const mainController = require('../controllers/main.controller');

//HTTP Vverbs: POST, GET, PU,DELET

//POST /providers
router.post('/providers', mainController.create);

//GET /api/providers

router.get('/providers', mainController.readAll);

// GET one /api/providers/123

router.get('/providers/:id',mainController.readOne);

// Put /api/provider/123

router.put('/providers/:id', mainController.update);

// Delte one provider /api/providers/123

router.delete('/providers/:id', mainController.deleteOne);

// Dleete all provider /api/providers
router.delete('/providers', mainController.deleteAll);

// nont matichin round
router.post('/*',notFound)
router.get('/*',notFound)
router.put('/*',notFound)
router.delete('/*',notFound)



function notFound(req,res){
res.status(404);
res.send('Not valid endpoint')

}
module.exports = router;
